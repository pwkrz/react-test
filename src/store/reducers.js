import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { getUsersArray } from '../utils/LocalStorageManager';
import { ActionTypes } from './actions';
import rootSaga from './sagas';

const getInitialUserState = () => {
    const usersArr = getUsersArray();
    const loggedInUser = (usersArr || []).filter( user => user.loggedIn )[0];
    return {
        authenticated: !!loggedInUser,
        errorMessage: ''
    }
};

const initialUserState = getInitialUserState();

const localeReducer = (state = {locale: 'it-IT'}, action) => {
    switch (action.type) {
        case ActionTypes.CHANGE_LOCALE:
            return { locale: action.locale };
        default:
            return state;
    }
};

const userReducer = (state = initialUserState, action) => {
    switch (action.type) {
        case ActionTypes.ERROR:
        case ActionTypes.SUCCESS:
            return action.data;
        default:
            return state;
    }
};

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
    users: userReducer,
    locales: localeReducer
});

const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;
