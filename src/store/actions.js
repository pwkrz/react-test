export const ActionTypes = {
    LOGIN: 'LOGIN',
    REGISTER: 'REGISTER',
    LOGOUT: 'LOGOUT',
    ERROR: 'ERROR',
    SUCCESS: 'SUCCESS',
    CHANGE_LOCALE: 'CHANGE_LOCALE'
};

export const changeLocale = (locale) => {
    return {
        type: ActionTypes.CHANGE_LOCALE,
        locale
    }
};

export const loginUser = (userData) => {
    return {
        type: ActionTypes.LOGIN,
        userData
    }
};

export const registerUser = (userData) => {
    return {
        type: ActionTypes.REGISTER,
        userData
    }
};

export const logoutUser = () => {
    return {
        type: ActionTypes.LOGOUT
    }
};
