import { EMAIL_REGEX, getEncodedPassword } from '../../utils/Helpers';
import { put } from 'redux-saga/effects';
import { ActionTypes } from '../actions';
import { getUserByEmail, saveLoggedInState, saveToUsersArray } from '../../utils/LocalStorageManager';

export default function* (action) {
    let { email, password, passwordVerification } = action.userData;
    const emailIsValid = yield EMAIL_REGEX.test(email);
    if (!emailIsValid) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                errorMessage: 'shared.invalid_email',
                authenticated: false
            }
        });
        return;
    }
    const emailTaken = yield !!getUserByEmail(action.userData.email);
    if (emailTaken) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                errorMessage: 'shared.emailTaken',
                authenticated: false
            }
        });
        return;
    }
    const pwdIsValid = yield password.length > 7;
    if (!pwdIsValid) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                errorMessage: 'shared.passwordTooShort',
                authenticated: false
            }
        });
        return;
    }
    const pwdsMatch = yield password === passwordVerification;
    if (!pwdsMatch) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                errorMessage: 'shared.passwordsNotMatch',
                authenticated: false
            }
        });
        return;
    }
    password = yield getEncodedPassword(password);
    yield saveToUsersArray({ email, password });
    yield saveLoggedInState(email);
    yield put({
        type: ActionTypes.SUCCESS,
        data: {
            errorMessage: '',
            authenticated: true
        }
    });
}