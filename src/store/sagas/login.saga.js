import { getEncodedPassword } from '../../utils/Helpers';
import { put } from 'redux-saga/effects';
import { ActionTypes } from '../actions';
import { getUserByEmail, saveLoggedInState } from '../../utils/LocalStorageManager';

export default function* (action) {
    let { email, password } = action.userData;
    const user = getUserByEmail(email);
    const encodedPwd = yield getEncodedPassword(password);
    const passwordsMatch = user && encodedPwd === user.password;
    if (!user || !passwordsMatch) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                authenticated: false,
                errorMessage: 'auth.loginerror'
            }
        });
        return;
    }
    yield saveLoggedInState(email);
    yield put({
        type: ActionTypes.SUCCESS,
        data: {
            errorMessage: '',
            authenticated: true
        }
    });
}