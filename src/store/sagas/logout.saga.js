import { saveLoggedInState } from '../../utils/LocalStorageManager';
import { put } from 'redux-saga/effects';
import { ActionTypes } from '../actions';

export default function* () {
    const saveResult = yield saveLoggedInState(null);
    if (!saveResult) {
        yield put({
            type: ActionTypes.ERROR,
            data: {
                errorMessage: 'shared.errorLogingOut',
                authenticated: true
            }
        });
        return;
    }
    yield put({
        type: ActionTypes.SUCCESS,
        data: {
            errorMessage: '',
            authenticated: false
        }
    });
}