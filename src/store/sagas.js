import { ActionTypes } from './actions';
import { takeLatest, all } from 'redux-saga/effects';
import logoutUserSaga from './sagas/logout.saga';
import registerUserSaga from './sagas/register.saga';
import loginUserSaga from './sagas/login.saga';

export default function* rootSaga() {
    yield all([
        takeLatest(ActionTypes.LOGOUT, logoutUserSaga),
        takeLatest(ActionTypes.LOGIN, loginUserSaga),
        takeLatest(ActionTypes.REGISTER, registerUserSaga)
    ])
}