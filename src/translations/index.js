import en from './en'
import it from './it'

export const messages = {
    'en-US': en,
    'it-IT': it
};

export const supportedLocales = Object.keys(messages).map(key => ({
    key,
    displayName: key.split('-')[0]
}));
