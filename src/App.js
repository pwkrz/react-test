import React, { Component } from 'react';
import './App.css';
import HomePage from "./ui/pages/Home/Home";
import LoginPage from "./ui/pages/Login/Login";
import RegisterPage from './ui/pages/Register/Register';
import { Wrapper } from "./ui/elements/Wrapper";
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/reducers';
import NavigationBar from "./ui/components/NavigationBar/NavigationBar";
import { addLocaleData, IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import { changeLocale } from './store/actions';
import enLocaleData from 'react-intl/locale-data/en';
import itLocaleData from 'react-intl/locale-data/it';
import { supportedLocales, messages } from './translations/index'
import PropTypes from 'prop-types';

addLocaleData(enLocaleData);
addLocaleData(itLocaleData);

class BaseApp extends Component {
    render() {
        return (
            <IntlProvider locale={this.props.locale} messages={messages[this.props.locale]}>
                <Router>
                    <Wrapper height="100vh" flexDirection="column">
                        <NavigationBar locales={supportedLocales}
                                       selectedLocale={this.props.locale}
                                       handleLocaleChange={this.props.handleLocaleChange} />
                        <Switch>
                            <Route path="/login" component={ LoginPage } />} />
                            <Route path="/register" component={ RegisterPage } />} />
                            <Route path="/home" component={ HomePage } />
                            <Route render={() => (<Redirect to="/login" />)} />
                        </Switch>
                    </Wrapper>
                </Router>
            </IntlProvider>
        );
    }
}

const mapStateToProps = (state) => {
    return ({
        locale: state.locales.locale
    })
};

const mapDispatchToProps = {
    handleLocaleChange: changeLocale
};

const ConnectedApp = connect(
    mapStateToProps,
    mapDispatchToProps
)(BaseApp);

export default function() {
    return (
        <Provider store={store}><ConnectedApp/></Provider>
    )
};

BaseApp.propTypes = {
    locale: PropTypes.string.isRequired,
    handleLocaleChange: PropTypes.func.isRequired
};
