import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

export const Input = styled.input`
  font-weight: 600;
  padding: 0.5em;
  margin: ${props => props.margin};
  border-radius: 3px;
  width: ${props => props.width};
`;
storiesOf('Input', module)
    .add('Normal', () => (
        <Input/>
    ));