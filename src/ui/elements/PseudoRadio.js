import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

export const PseudoRadio = styled.ul`
    display: flex;
    justify-content: space-evenly;
    color: #aaa;
    border: 1px solid;
    border-radius: 3px;
    cursor: pointer;
    padding: 0 .25em;
    margin: 1em;
    
    > li {
        display: inline-block;
        padding: .25em;
        
        &:hover {
            background-color: #eee;
        }
        &.selected {
            color: #111;
            text-decoration: underline;
        }
    }
`;

storiesOf('PseudoRadio', module)
    .add('Normal', () => (
        <PseudoRadio>Normal PseudoRadio</PseudoRadio>
    ));