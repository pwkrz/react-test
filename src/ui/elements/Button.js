import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

export const Button = styled.button`
  color: ${props => props.primary ? 'black' : 'white'};
  font-size: ${props => props.small ? '.75em' : '1em'};
  font-weight: 600;
  margin: ${props => props.margin};
  padding: 0.5em 1em;
  border-radius: 3px;
  cursor: pointer;
  opacity: ${props => props.disabled ? .5 : 1};
`;

storiesOf('Button', module)
    .add('Normal', () => (
        <Button>Normal Button</Button>
    ))
    .add('Primary', () => (
        <Button primary>Primary Button</Button>
    ))
    .add('Small', () => (
        <Button small>Small Button</Button>
    ))
    .add('Disabled', () => (
        <Button disabled primary>Disabled Primary Button</Button>
    ));