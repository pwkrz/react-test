import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    flex-direction: ${props => props.flexDirection};
    justify-content: ${props => props.justifyContent};
    align-items: ${props => props.alignItems};
    width: ${props => props.width || '100%'};
    height: ${props => props.height};
    margin: ${props => props.margin};
    padding: ${props => props.padding};
    flex-grow: ${props => props.flexGrow};
`;
storiesOf('Wrapper', module)
    .add('Normal', () => (
        <Wrapper>Normal Wrapper</Wrapper>
    ));