import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

export const PageHeader = styled.h1`
    font-size: ${props => props.fontSize}
    width: ${props => props.width};
    height: ${props => props.height};
    margin: ${props => props.margin};
    padding: ${props => props.padding};
`;
storiesOf('PageHeader', module)
    .add('Normal', () => (
        <PageHeader>Normal Header</PageHeader>
    ));