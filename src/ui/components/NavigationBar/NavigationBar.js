import React from 'react';
import { Wrapper } from "../../elements/Wrapper";
import { PseudoRadio } from '../../elements/PseudoRadio';

export default function(props) {
    const pseudoRadioChildren = props.locales.map( (locale, i) => (
        <li key={'locale-'+i}
            onClick={() => props.handleLocaleChange(locale.key)}
            className={locale.key === props.selectedLocale ? 'selected' : ''}
        >
            { locale.displayName }
        </li>
    ));
    return (
        <Wrapper justifyContent="center" alignItems="center" height="4em">
            <PseudoRadio>
                { pseudoRadioChildren }
            </PseudoRadio>
        </Wrapper>
    )
}