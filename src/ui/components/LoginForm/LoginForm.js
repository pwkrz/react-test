import React, { Component } from 'react';
import { Input } from '../../elements/FormControls';
import { Wrapper } from '../../elements/Wrapper';
import { Button } from '../../elements/Button';
import { connect } from 'react-redux';
import { loginUser, registerUser } from '../../../store/actions';
import { getResetFormState } from '../../../utils/Helpers';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

class LoginForm extends Component {
    state = getResetFormState();
    handleSubmit(forLoginPage) {
        let { email, password, passwordVerification } = this.state;
        this.setState({ error: '' });
        if (forLoginPage) {
            this.props.loginUser({ email, password });
        } else {
            this.props.registerUser({ email, password, passwordVerification });
        }
    }
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if (this.props.error.length
            && prevState.email === this.state.email
            && prevState.password === this.state.password
            && prevState.passwordVerification === this.state.passwordVerification
            && this.props.intl.locale === prevProps.intl.locale) {
            alert(this.props.intl.formatMessage({ id: this.props.error }));
        }
        if (this.props.forLoginPage !== prevProps.forLoginPage) {
            this.setState( getResetFormState() )
        }
        return null;
    };
    componentDidUpdate() {
    }
    render() {
        return (
            <Wrapper width="500px" margin="2em 0" flexDirection="column">
                <Input
                    type="email" margin="0 0 1.5em"
                    placeholder={this.props.intl.formatMessage({ id: 'shared.email' })}
                    value={this.state.email}
                    onChange={(e) => this.setState({ email: e.target.value})}
                />
                <Input
                    type="password" margin="0 0 1.5em"
                    placeholder={this.props.intl.formatMessage({ id: 'shared.password' })}
                    value={this.state.password}
                    onChange={(e) => this.setState({ password: e.target.value})}
                />
                {   !this.props.forLoginPage &&
                    <Input
                        type="password" margin="0 0 1.5em"
                        placeholder={this.props.intl.formatMessage({ id: 'shared.repeatPassword' })}
                        value={this.state.passwordVerification}
                        onChange={(e) => this.setState({ passwordVerification: e.target.value})}
                    />
                }
                <Button primary
                    onClick={() => this.handleSubmit(this.props.forLoginPage)}
                >
                    {this.props.forLoginPage ?
                        this.props.intl.formatMessage({ id: 'auth.login' }) :
                        this.props.intl.formatMessage({ id: 'auth.register' })}
                </Button>
            </Wrapper>
        );
    }
}

const mapDispatchToProps = {
    loginUser,
    registerUser
};

export default connect(null, mapDispatchToProps)(injectIntl(LoginForm));

LoginForm.propTypes = {
    loginUser: PropTypes.func.isRequired,
    registerUser: PropTypes.func.isRequired,
    forLoginPage: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    intl: PropTypes.any.isRequired
};
