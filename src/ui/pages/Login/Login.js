import React, { Component } from 'react';
import { Wrapper } from "../../elements/Wrapper";
import LoginForm from '../../components/LoginForm/LoginForm';
import { PageHeader } from '../../elements/PageHeader';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

class Login extends Component {
    render() {
        const { error, isLoggedIn, intl } = this.props;
        if (isLoggedIn) {
            return (
                <Redirect to="/home"/>
            )
        } else {
            return (
                <Wrapper flexDirection="column" justifyContent="center" flexGrow="1" margin="-10% 0 0 0">
                    <Wrapper justifyContent="center">
                        <PageHeader>{ intl.formatMessage({ id: 'login.welcome' }) }</PageHeader>
                    </Wrapper>
                    <Wrapper flexDirection="column" alignItems="center" justifyContent="center">
                        <LoginForm forLoginPage={true} error={error} />
                        <Link to="/register">{ intl.formatMessage({ id: 'login.anchorText' }) }</Link>
                    </Wrapper>
                </Wrapper>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.users.authenticated,
        error: state.users.errorMessage,
    }
};

export default connect(
    mapStateToProps
)(injectIntl(Login))

Login.propTypes = {
    error: PropTypes.string.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    intl: PropTypes.any.isRequired
};
