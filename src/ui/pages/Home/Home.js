import React, { Component } from 'react';
import { Wrapper } from '../../elements/Wrapper';
import { PageHeader } from '../../elements/PageHeader';
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import { logoutUser } from '../../../store/actions';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

class Home extends Component {
    state = {
        locale: this.props.intl.locale
    };
    static getDerivedStateFromProps(props, state) {
        if (props.error.length &&
            props.intl.locale === state.locale) {
            alert(props.intl.formatMessage({ id: props.error }));
        }
        return null;
    }
    render() {
        if (this.props.isLoggedIn) {
            return (
                <Wrapper flexDirection="column" alignItems="center" justifyContent="center" flexGrow="1" padding="0 0 10% 0">
                    <PageHeader>
                        {this.props.intl.formatMessage({ id: 'home.welcome' })}
                    </PageHeader>
                    <Link to='/login' onClick={this.props.logoutUser}>
                        {this.props.intl.formatMessage({ id: 'auth.logout' })}
                    </Link>
                </Wrapper>
            );
        } else {
            return (
                <Redirect to="/login"/>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.users.authenticated,
        error: state.users.errorMessage
    }
};

const mapDispatchToProps = {
    logoutUser
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(injectIntl(Home));

Home.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    intl: PropTypes.any.isRequired
};
