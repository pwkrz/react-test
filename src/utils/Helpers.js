export const EMAIL_REGEX = /^[a-z0-9._%+!$&*=^|~#%'`?{}/\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,16})$/i;

export const getResetFormState = () => ({
    email: '',
    password: '',
    passwordVerification: '',
    error: ''
});

export const getEncodedPassword = pwd => {
    return btoa(pwd);
};

export const getDecodedPassword = pwd => {
    return atob(pwd);
};
