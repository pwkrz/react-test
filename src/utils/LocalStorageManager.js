export const saveToUsersArray = (userObj) => {
    const usersArr = getUsersArray() || [];
    usersArr.push(userObj);
    const dataStr = JSON.stringify(usersArr);
    localStorage.setItem('users', dataStr);
};

export const saveLoggedInState = (email) => {
    const usersArr = getUsersArray();
    const updatedUsersArr = (usersArr || []).map(user => user.email === email ? ({...user, loggedIn: true}) : ({...user, loggedIn: false}));
    const dataStr = JSON.stringify(updatedUsersArr);
    localStorage.setItem('users', dataStr);
    return localStorage.getItem('users') !== null;
};

export const getUsersArray = () => {
    const dataStr = localStorage.getItem('users');
    const dataArr = JSON.parse(dataStr);
    return dataArr && Array.isArray(dataArr) ? dataArr : false;
};

export const getUserByEmail = (email) => {
    const usersArr = getUsersArray();
    if (!usersArr) {
        return false;
    }
    const user = usersArr.filter( user => user.email === email )[0];
    return user;
};

export const deleteUsers = () => {
    localStorage.removeItem('users');
};

export const deleteUserByEmail = (email) => {
    const usersArr = getUsersArray();
    if (!usersArr) {
        return false;
    }
    const updatedUsersArr = usersArr.filter( user => user.email !== email );
    const updatedUsersStr = JSON.stringify(updatedUsersArr);
    localStorage.setItem('users', updatedUsersStr);
    return true;
};
